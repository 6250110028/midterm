
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/model/profile_model.dart';

class ShowProfile extends StatelessWidget {
  final id;
  ShowProfile({Key? key, required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('รายละเอียดโปรไฟล์'),
      ),
      body: Center(

        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                children: snapshot.data!.map((profile) {
                  return Center(
                    child: Column(
                            children: [
                         Container(
                           margin: EdgeInsets.only(top: 30,),
                         ),
                        CircleAvatar(
                          backgroundImage: FileImage(File(profile.image)),
                          radius: 100,
                        ),
                        Container(
                          margin: EdgeInsets.all(20.00),
                          child: Column(
                            children: [
                          Text('${profile.firstname}  ${profile.lastname}',
                                style:
                                TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                              ),

                              Text('Email: ${profile.email}',
                                style:
                                TextStyle(fontSize: 20,),
                              ),
                              Text('Phone: ${profile.phone}',
                                style:
                                TextStyle(fontSize: 20,),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
