import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'package:sqlite_basic/pages/wannaporn.dart';
import 'package:sqlite_basic/pages/woralak.dart';
import 'database/database_helper.dart';
import 'pages/edit_profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: const MyHomePage(title: 'คอร์สเรียนทำเบเกอรี่'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int? selectedId;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AddProfile()),
                  ).then((value){
                    //getAllData();
                    setState(() {

                    });
                  });
                }, child: Text('สมัครที่นี้',
              style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold,
              ),
              ),

              ),
          ),
        ],
      ),

    body: Center(
      child: FutureBuilder<List<ProfileModel>>(
          future: DatabaseHelper.instance.getGroceries(),
          builder: (BuildContext context,
              AsyncSnapshot<List<ProfileModel>> snapshot) {
            if (!snapshot.hasData) {
              return Center(child: Text('Loading...'));
            }
            return snapshot.data!.isEmpty
                ? Center(child: Text('ยังไม่มีข้อมูลการสมัคร'))
                : ListView(
              children: snapshot.data!.map((grocery) {
                return Center(
                  child: Card(
                    color: selectedId == grocery.id
                        ? Colors.white70
                        : Colors.white,
                    child: ListTile(
                      title: Text('${grocery.firstname} ${grocery.lastname}'),
                      subtitle: Text(grocery.email),
                      leading: CircleAvatar(backgroundImage: FileImage(File(grocery.image))),
                      //leading: Image(
                      //  image: FileImage(File(grocery.image)),
                      //  fit: BoxFit.cover,
                      //  height: 500,
                      //  width: 100,
                      //),
                        trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children:  <Widget>[
                              new IconButton(
                                padding: EdgeInsets.all(0),
                                icon: Icon(Icons.edit),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => EditProfile(grocery)),
                                  ).then((value){
                                    setState(() {
                                    });
                                  });
                                },
                              ),
                              new IconButton(
                                padding: EdgeInsets.all(0),
                                icon: Icon(Icons.clear),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: new Text("คุณต้องการลบข้อมูลนี้หรือไม่?"),
                                       // content: new Text("Please Confirm"),
                                        actions: [
                                          new TextButton(
                                            onPressed: () {
                                               DatabaseHelper.instance.remove(grocery.id!);
                                               setState(() {
                                                   Navigator.of(context).pop();
                                                 });
                                            },
                                            child: new Text("ตกลง"),
                                          ),
                                          Visibility(
                                            visible: true,
                                            child: new TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: new Text("ยกเลิก"),
                                            ),
                                          ),
                                        ],
                                      );
                                    },
                                  );

                                },
                              ),
                            ],
                        ),
                      onTap: () {
                        var profileid = grocery.id;
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ShowProfile(id: profileid)));

                        setState(() {
                          print(grocery.image);
                          if (selectedId == null) {
                            //firstname.text = grocery.firstname;
                            selectedId = grocery.id;
                          } else {
                           // textController.text = '';
                            selectedId = null;
                          }
                        });
                      },
                      onLongPress: () {
                        setState(() {
                          DatabaseHelper.instance.remove(grocery.id!);
                        });
                      },
                    ),
                  ),
                );
              }).toList(),
            );
          }),
    ),
      drawer: Drawer(
        child: ListView(padding: const EdgeInsets.all(0.0), children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text('About',style: TextStyle(
                fontSize: 25
            ),),
            accountEmail: const Text('สมาชิกในกลุม'),

          ),
          ListTile(
              title: const Text('Wannaporn Thongkaw'),
              leading: const Icon(Icons.account_circle_sharp),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder:  (context) => const Wannaporn()));
              }),
          ListTile(
              title: const Text('Woralak Samart'),
              leading: const Icon(Icons.account_circle_sharp),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder:  (context) => const Woralak()));
              }),
          const Divider(),
          ListTile(
              title: const Text('ปิด'),
              leading: const Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ]),
      ),
    );
  }
}

